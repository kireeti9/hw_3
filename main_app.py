from flask import Flask, request, flash, url_for, redirect, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///students.sqlite3'
app.config['SECRET_KEY'] = "random string"

db = SQLAlchemy(app)

class students(db.Model):
    id = db.Column('student_id', db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    grade = db.Column(db.Integer())

    def __init__(self, name, grade):
        self.name = name
        self.grade = grade

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/submit_grade')
def submit_grade():
    student_name = request.args.get('student_name')
    grade = request.args.get('grade')

    new_student = students(student_name, grade)
    db.session.add(new_student)
    db.session.commit()
    flash('Record was successfully added')
    return render_template('home.html', student_name=new_student.id)

@app.route('/edit_existing_grade')
def edit_existing_grade():
    student_name = request.args.get('student_name')
    grade = request.args.get('grade')

    student = students.query.filter_by(name=student_name).first()
    student.grade = grade
    db.session.commit()
    flash('Record successfully edited')
    return render_template('home.html')

@app.route('/edit_grade/<student_name>/<grade>')
def edit_grade(student_name, grade):
    return render_template('edit_grade.html', student_name=student_name, grade=grade)\

@app.route('/delete_record')
def delete_record():
    student_id = request.args.get('student_id')
    del_student = students.query.get(student_id)

    db.session.delete(del_student)
    db.session.commit()
    flash('Record successfully deleted')
    return render_template('home.html')

@app.route('/show_all')
def show_all():
    return render_template('show_all.html', students = students.query.all() )

@app.route('/show_greater_than_85')
def show_greater_than_85():
    return render_template('show_all.html', students = students.query.filter(students.grade > 85).all())

if __name__ == '__main__':
    db.create_all()
    app.run(debug = True)
